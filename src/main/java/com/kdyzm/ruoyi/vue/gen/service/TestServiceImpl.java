package com.kdyzm.ruoyi.vue.gen.service;

import com.kdyzm.ruoyi.vue.gen.domain.GenTable;
import com.kdyzm.ruoyi.vue.gen.mapper.GenTableMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author kdyzm
 */
@Service
@Slf4j
public class TestServiceImpl implements TestService {


    @Autowired
    private GenTableMapper genTableMapper;

    @Override
    public void test() {
        List<GenTable> genTables = genTableMapper.selectGenTableAll();
        log.info("获取所有数量={}", genTables.size());
    }
}
