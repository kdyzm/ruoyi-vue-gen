package com.kdyzm.ruoyi.vue.gen;

import com.kdyzm.ruoyi.vue.gen.config.Config;
import com.kdyzm.ruoyi.vue.gen.manager.GenManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author kdyzm
 */
@Slf4j
public class Main {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(Config.class);
        ctx.refresh();

        GenManager bean = ctx.getBean(GenManager.class);
        bean.generate();
        log.info("生成结束，ruoyi.zip为生成文件");
    }
}
